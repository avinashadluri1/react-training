import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Child from './Child'; 
class App extends Component {
  render() { 
    const mystyles = {color:'white', fontSize:'34px'};
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title" style={mystyles}>Welcome to React</h1>
        </header>
        <Child />
      </div>
    );
  }
}

export default App;
